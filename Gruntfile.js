module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),


    // Minify files with UglifyJS
    uglify: {
      options: {
      },
      my_target: {
        files: {
          'source/Build/js/vendor/modernizr-2.7.1.min.js':['source/assets/scripts/vendor/modernizr-2.7.1.min.js'],
          'source/Build/js/vendor/jquery-1.11.0.min.js':['source/assets/scripts/vendor/jquery-1.11.0.min.js'],
          'source/Build/js/vendor/selectivizr.min.js': ['source/assets/scripts/vendor/selectivizr.min.js'],
          'source/Build/js/vendor/html5shiv.min.js': ['source/assets/scripts/vendor/html5shiv.min.js'],
          'source/Build/js/vendor/placeholder_polyfill.jquery.min.js': ['source/assets/scripts/vendor/placeholder_polyfill.jquery.min.js'],          
          'source/Build/js/fonts.js': ['source/assets/scripts/fonts.js'],
          'source/Build/js/plugins.js':['source/assets/scripts/jquery.magnific-popup.js'],
          'source/Build/js/main.js': ['source/assets/scripts/main.js']
        }
      }
    },

    // Clean files and folders
    clean: ['source/Build'],


    // Compile Sass to CSS
    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'source/Build/css/main.css': 'source/assets/styles/main.scss',
          'source/Build/css/placeholder_polyfill.min.css': 'source/assets/styles/placeholder_polyfill.min.scss'
        }
      }
    },

    // Minify PNG and JPEG images
    imagemin: {
      dist: {
        options: {
          optimizationLevel: 3,
          progressive: true
        },
        files: [{
            expand: true,
            cwd: 'source/assets/images',
            src: '{,*/,*/*/}*.{png,jpg,jpeg}',
            dest: 'source/Build/img'
        }]
      },
    },

    // Copy files and folders
    copy: {
      dist: {
        files: [
          {src: ['source/Build/css/*'], dest: 'code/wp-content/themes/bakology/css/', expand:true, flatten:true},
          {src: ['source/Build/img/*'], dest: 'code/wp-content/themes/bakology/img/',expand:true, flatten:true},
          {src: ['source/Build/fonts/*'], dest: 'code/wp-content/themes/bakology/fonts/',expand:true, flatten:true},
          {src: ['source/Build/js/*'], dest: 'code/wp-content/themes/bakology/js/',expand:true, flatten:true},
          {src: ['source/Build/js/vendor/*'], dest: 'code/wp-content/themes/bakology/js/vendor/',expand:true, flatten:true}
      ]
      },
      dev: {
        files: [
          {src: ['source/assets/favicon.ico'], dest: 'source/Build/', expand:true, flatten:true},
          //{cwd: 'source/assets/images/',src: ['**'], dest: 'source/Build/img/',expand:true, flatten:true},
          {cwd: 'source/assets/fonts/',src: ['**'], dest: 'source/Build/fonts/',expand:true, flatten:true},
          {src: ['source/*.html'], dest: 'source/Build/',expand:true, flatten:true},
          {src: ['source/assets/images/*.gif'], dest: 'source/Build/img',expand:true, flatten:true}
        ]
      }
    },

    connect: {
      server: {
        options: {
          port: 9001,
          base: 'source/Build/'
        }
      }
    },

    // Run predefined tasks whenever watched file patterns are added, changed or deleted
    watch: {
      options: {
        livereload: true
      },
      js: {
        files: ['source/assets/scripts/*.js','source/assets/scripts/*/*.js'],
        tasks: ['build','deploy'],
        events: ['all']
      },
      css: {
        files: ['source/assets/styles/*.scss','source/assets/styles/*/*.scss'],
        tasks: ['build','deploy'],
        events: ['all']
      },
      html: {
        files: ['source/*.html'],
        tasks: ['build','deploy'],
        events: ['all']
      },
      img: {
        files: ['source/assets/images/*'],
        tasks: ['build','deploy'],
        events: ['all']
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-imagemin');

  grunt.registerTask('build', ['clean','uglify','sass','copy:dev','imagemin']);
  grunt.registerTask('deploy', ['copy:dist']);
  grunt.registerTask('default', ['connect','watch']);

};
