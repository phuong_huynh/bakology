<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bakology');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.^(RoiWC]YJHcFi33}gUkEmxh<ZkuuW1[`e}a)+AzkDCPSc+pUQ.eGj<@SaY%8Cd');
define('SECURE_AUTH_KEY',  'gT/Mz2tq?c>u[~]S5 :t&*iHk/3p~k_SYE,}E=k7fv#:(@SWW>IWSxN;caCFd&jE');
define('LOGGED_IN_KEY',    'j5m$>2nB.K0SRl0k0Y<*$.zyCtWbiqN]#DR+1&(&ExI]?Ya;E7fp%_]?Gps</|8h');
define('NONCE_KEY',        '%]zE]/-z6pLs);wQKQoR+6sSY;#!?p&&8;PDo<zZJu>X%&;:&IuW&o+SAAuGqv6s');
define('AUTH_SALT',        'u!+us0J,t5no8*?I(^}|nxVQ1-4s03E%]gZ#f>2}GG67d:J(Nn&vVc]Ct>T3*0*:');
define('SECURE_AUTH_SALT', '/w+yo})(W}czPt [IwqYA;o&H+)IX3Ebe^QiD5Ui|`r+axJ}7Cv`gQ0%SM:nVdx#');
define('LOGGED_IN_SALT',   'KY&]%j-#g_sv#dX-3=+rGP0m`Nz($vH%mE|f2DpC2e|r$_N5vpElY3Ep+e7`rW$$');
define('NONCE_SALT',       '@qxwDLRXC^qC`%?E`Ka| <h?}xNli])TtWb7g0mn09@TR%dp<9<1Nweh+z.(599a');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bak_';

/**
 * WordPress Localized Language, defaults to Australian English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * en_AU.mo to wp-content/languages and set WPLANG to 'en_AU' to enable
 * Australian English language support.
 */
define('WPLANG', 'en_AU');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
