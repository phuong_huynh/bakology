��    R      �  m   <      �     �  r        z     �     �     �     �  /   �       &        E     X     q     w  >   �     �  "   �  J   
	     U	     j	     �	     �	     �	     �	     �	     �	  	   �	  -   �	  /   #
  <   S
     �
     �
     �
     �
  3   �
  P   �
  a   F     �     �     �     �     �     �     �     �                              !     &  
   /     :     R     W     `     g     j     r     �     �     �     �     �     �  	   �     �     �     �     �     �     �           	                    $     (     .  �  2  $   	  �   .     �     �                  -   2     `  0   t     �     �     �     �  B   �     1  *   H  B   s     �     �     �     �               !     6     Q  3   `  9   �  I   �  
        #     0     =  :   S  W   �  Z   �     A     O  
   W     b     q     �     �     �     �     �     �     �     �     �     �     �       
        *     ;     O     S     Z     k  
   q     |     �     �     �     �     �     �     �     �     �  
             %     2  
   7     B     O     S     Z        $   P      4       R   L   O          Q   3          <   E   '   I       =      2                  8       0                    B   K      !           	   /   D              @   )              C         "          6           A   J   *      F   N   ;       H   1   7   M                     >       &   (          .      G   #                   5   -   %       :      ?         ,       
   9   +       Block Direct Comments Blocks direct access to wp-comments-post.php. Enable this to prevent spambots from posting to Wordpress via a URL. Blog about it & link to the Captcha field title Captcha time Captcha time expired. Check out our other Copy this code and paste it into the form left. Deactivation Delete settings on plugin deactivation Display captcha as Do you like this plugin? ERROR Enable Math Captcha for Enter the time (in seconds) a user has to enter captcha value. Hide for logged in users How to entitle field with captcha? If you are having problems with this plugin, please talk about them in the Input field settings Invalid captcha value. Math Captcha Math Captcha settings Mathematical operations Name Need support? Please enter captcha value. Rate it 5 Select how you'd like to display you captcha. Select were would you like to use Math Captcha. Select which mathematical operations to use in your captcha. Settings Support Support forum WordPress plugins Would you like to hide captcha for logged in users? You need to check at least one group. Defaults settings of this option restored. You need to check at least one mathematical operation. Defaults settings of this option restored. addition (+) bbpress comment form contact form 7 division (&#247;) eight eighteen eighty eleven fifteen fifty five forty four fourteen login form multiplication (&#215;) nine nineteen ninety no numbers on WordPress.org one optional plugin page registration form reset password form seven seventeen seventy six sixteen sixty subtraction (-) ten thirteen thirty three twelve twenty two words yes Project-Id-Version: Math Captcha
POT-Creation-Date: 2014-01-14 14:53+0100
PO-Revision-Date: 2014-01-14 14:54+0100
Last-Translator: Bartosz Arendt <info@digitalfactory.pl>
Language-Team: dFactory <info@dfactory.eu>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.3
X-Poedit-KeywordsList: gettext;gettext_noop;__;_e
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 Blokowanie bezpośrednich komentarzy To ustawienie blokuje bezpośredni dostęp do pliku wp-comments-post.php. Włącz to jeśli chcesz uniemożliwiść spambotom dodawanie komentarzy poprzez URL. Napisz o niej i dodaj link do Tytuł pola captcha Czas captcha Upłynął czas. Sprawdź nasze inne Skopij i wkej ten kod do formularza po lewej. Deaktywacja wtyczki Usuń wszystkie dane wtyczki podczas deaktywacji Wyświetlaj captcha jako Lubisz tę wtyczkę? BŁĄD Włącz captcha dla Podaj czas (w sekundach) w jakim należy uzupełnić pole captcha. Ukryj dla zalogowanych Jaki tytuł będzie miało pole z captcha? Jeśli masz jakiekolwiek problemy z tą wtyczką, powiedz o tym na Ustawienia pola Nieprawidłowa wartość. Math Captcha Ustawienia Math Captcha Operacje matematyczne Nazwa Potrzebujesz pomocy? Proszę wpisać wartość. Oceń ją na 5 Wybierz w jaki sposób chcesz wyświetlać captcha. Wybierz w których miejscach chcesz używać Math Captcha Wybierz jakie rodzaje operacji matematycznych będą stosowane w captcha. Ustawienia Forum pomocy Forum pomocy wtyczki do WordPressa Czy chcesz ukryć captcha dla zalogowanych użytkowników? Musisz wybrać przynajmniej jedną grupę. Przywrócono domyślne ustawienie tej opcji. Musisz wybrać przynajmniej jedną operację. Przywrócono domyślne ustawienie tej opcji. dodawanie (+) bbpress komentarze contact form 7 dzielenie (&#247;) osiem osiemnaście osiemdziesiąt jedenaście piętnaście pięćdziesiąt pięć czterdzieści cztery czternaście formularz logowania mnożenie (&#215;) dziewięć dziewiętnaście dziewięćdziesiąt nie liczby na WordPress.org jeden opcjonalne strony wtyczki formularz rejestracji resetowanie hasła siedem siedemnaście siedemdziesiąt sześć szesnaście sześćdziesiąt odejmowanie (-) dziesięć trzynaście trzydzieści trzy dwanaście dwadzieścia dwa słowa tak 