	</section>
</div>
<div class="wrapper">
		<footer>
				<div class="footer__content">
						Copyright BAK. 2014<br />
						Website by <a href="#">redmeetsblue</a>
				</div>
		</footer>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/selectivizr.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/placeholder_polyfill.jquery.min.js"></script>
<![endif]-->
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
		(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
		function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
		e=o.createElement(i);r=o.getElementsByTagName(i)[0];
		e.src='//www.google-analytics.com/analytics.js';
		r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
		ga('create','UA-XXXXX-X');ga('send','pageview');
</script>
</body>
</html>
