<?php get_header(); ?>

    <article>
        <header>
            <h2>Latest Video</h2>
        </header>
        <?php
            $latest_video_post = get_post( 21 );
            $playlist_id = get_playlist_id( $latest_video_post );
            $videos = get_videos( $playlist_id );
        ?>
        <div class="frame">
            <?php the_video( $videos[0], false )?>
        </div>
        <p class="readmore">
            <a href="<?php echo get_youtube_link( $latest_video_post );?>" target="_blank">View all videos</a>
        </p>
    </article>
    <article>
        <header>
            <h2>Latest Release</h2>
        </header>

        <?php

            $latest_EP_post = get_post(23);
            $EP_cover = get_EP_cover($latest_EP_post);
            $EP_link = get_EP_link($latest_EP_post);

        ?>

        <div class="image-container">
            <img src="<?php echo $EP_cover?>" />
        </div>
        <?php
            if( $latest_EP_post != null ):

                echo $latest_EP_post->post_content;

            endif;
        ?>
        <p class="readmore">
            <a href="<?php echo $EP_link?>">Listen to EP</a>
        </p>
    </article>

<?php get_footer(); ?>
