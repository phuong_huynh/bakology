<!doctype html>
<!--[if lt IE 7]>      <html class="ie6 ie"> <![endif]-->
<!--[if IE 7]>         <html class="ie7 ie"> <![endif]-->
<!--[if IE 8]>         <html class="ie8 ie"> <![endif]-->
<!--[if gt IE 8]><!--> <html>         <!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
		<!--[if lte IE 9]>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/placeholder_polyfill.jquery.min.css">
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/html5shiv.min.js"></script>
        <![endif]-->
 		<?php /*<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr-2.7.1.min.js"></script> */?>
		<?php //wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>
		<div class="wrapper">
			<aside class="aside">
				<div class="logo">
					<a href="<?php echo home_url(); ?>">Bakology</a>
				</div>
				<nav class="nav" role="navigation">
						<?php html5blank_nav(); ?>
				</nav>
				<div class="aside__box">
                    <h1>Bak's latest tweet</h1>
                    <?php the_latest_tweet(); ?>
                </div>
                <div class="aside__box">
                    <a href="https://twitter.com/intent/user?screen_name=bakofficial" class="button"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-icon.png" /> Follow @twitter</a>
                </div>
                <div class="aside__box social-media">
                    <ul>
                        <li class="facebook"><a href="http://www.facebook.com/bakofficial">facebook</a></li>
                        <li class="twitter"><a href="http://www.twitter.com/bakofficial">twitter</a></li>
                        <li class="youtube"><a href="http://www.youtube.com/bakofficial">youtube</a></li>
                        <li class="soundcloud"><a href="http://soundcloud.com/bakofficial">soundcloud</a></li>
                    </ul>
                </div>
			</aside>
			<section class="content">
