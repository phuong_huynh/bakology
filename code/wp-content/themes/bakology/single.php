<?php get_header(); ?>

	<article>
			<header>
					<h1><?php the_title(); ?></h1>
			</header>
			<?php


				if (have_posts()):
						while (have_posts()) : the_post();

							the_content();
							
						endwhile;
				endif;


		?>
	</article>

<?php get_footer(); ?>
