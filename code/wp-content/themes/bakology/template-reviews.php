<?php

	/* Template Name: Reviews template */

	/* This is a special page to display a video feed and sound cloud feed */

?>

<?php get_header(); ?>

	<article>
        <header>
            <h1><?php the_title(); ?></h1>
        </header>
        <div class="banner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/globe.png" />
        </div>
        <div class="intro">
            <h1>World Wide<br />Critical Acclaim</h1>
            <h2>from</h2>
        </div>                                        
        <div class="reviews__container">
        <?php

                $countries = get_countries( $args );
                
                foreach($countries as $country):
                   
					$custom_query = new WP_Query( 'cat=' . $country->cat_ID ); ?>
                    
                    <div class="reviews__country">
                        <h1><?php echo $country->name?></h1>
                    </div>
                    <div class="reviews">

                    
					<?php if( $custom_query->have_posts() ):?>

							<?php while( $custom_query->have_posts() ): $custom_query->the_post();

									$review_text = get_the_title();
									$review_caption = get_the_content();
									$site_name = get_field( "review_site_name" , get_the_id() );
									$site_url = get_field( "review_link" , get_the_id() );
									
                                    ?>
                                    
                                    <div class="review">
                                        <div class="review__text">
                                            <?php echo $review_text ?>
                                        </div>
                                        <div class="review__caption">
                                            <?php echo $review_caption?>
                                        </div>
                                        
                                        <?php if( $site_name != "" && $site_url != "" ): ?>
                                            
                                            <div class="review__info">
                                                <div class="review__info--site-name">
                                                    <?php echo $site_name?>
                                                </div>
                                                <div class="review__info--link">
                                                    <a href="<?php echo $site_url?>" target="_blank">Full Review</a>
                                                </div>
                                            </div>
                                                
                                        <?php endif; ?>
                                    </div>
                                    
                            <?php endwhile;

							wp_reset_postdata();

					endif; ?>
					
					</div>
					
				
				<?php endforeach;?>
        </div>
	</article>

<?php get_footer(); ?>
