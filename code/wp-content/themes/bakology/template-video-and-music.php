<?php

	/* Template Name: Video and Music Template */

	/* This is a special page to display a video feed and sound cloud feed */

?>

<?php get_header(); ?>

		<article>
				<header><h1><?php the_title(); ?></h1></header>
				<div id="content--video"  class="tab-1 active">
						<header>
								<h3>Videos</h3>
						</header>
						<div class="tab-content">
						<?php

								// video

								$video_post = get_post(25);
								$playlist_id = get_playlist_id($video_post);
								$videos = get_videos($playlist_id);


								for($i = 0; $i < count($videos); $i++) {
										the_video($videos[$i], true);
								}

						?>
						</div>
				</div>
				<div id="content--music" class="tab-2">
						<header>
								<h3>Music</h3>
						</header>
						<div class="tab-content">
								<?php

										// music


										$custom_query = new WP_Query( 'cat=3' );

										if( $custom_query->have_posts() ):

												while( $custom_query->have_posts() ): $custom_query->the_post();

														echo "<div class='music-container'>" . get_the_content() . "</div>";

												endwhile;

												wp_reset_postdata();

										endif;

								?>
						</div>
				</div>
	</article>


<?php get_footer(); ?>
